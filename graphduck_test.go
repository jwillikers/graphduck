package main

import (
	"errors"
	"fmt"
	"os"
	"testing"

	"gonum.org/v1/gonum/mat"
)

func TestGetArgLength(t *testing.T) {
	t.Run("arg=-1", func(t *testing.T) {
		os.Args = []string{"cmd", "-1"}
		_, err := getArgLength()
		if nil == err {
			t.Error("an error should have been returned")
		}
	})
	t.Run("arg=0", func(t *testing.T) {
		os.Args = []string{"cmd", "0"}
		_, err := getArgLength()
		if nil == err {
			t.Error("an error should have been returned")
		}
	})
	t.Run("arg=1", func(t *testing.T) {
		os.Args = []string{"cmd", "1"}
		_, err := getArgLength()
		if nil == err {
			t.Error("an error should have been returned")
		}
	})
	t.Run("arg=4", func(t *testing.T) {
		e := 4
		os.Args = []string{"cmd", "4"}
		l, err := getArgLength()
		if nil != err {
			t.Errorf("the error '%v' should not have been returned", err)
		}
		if e != l {
			t.Errorf("the url should be %d not %d", e, l)
		}
	})
	t.Run("arg=2.5", func(t *testing.T) {
		os.Args = []string{"cmd", "2.5"}
		_, err := getArgLength()
		if nil == err {
			t.Error("an error should have been returned")
		}
	})
	t.Run("arg=empty", func(t *testing.T) {
		os.Args = []string{"cmd", ""}
		_, err := getArgLength()
		if nil == err {
			t.Error("an error should have been returned")
		}
	})
	t.Run("arg=none", func(t *testing.T) {
		os.Args = []string{"cmd"}
		_, err := getArgLength()
		if nil == err {
			t.Error("an error should have been returned")
		}
	})
}

func TestGetArgFileName(t *testing.T) {
	t.Run("given", func(t *testing.T) {
		e := "filename.csv"
		os.Args = []string{"cmd", "www.google.com", e}
		fileName, err := getArgFileName()
		if nil != err {
			t.Errorf("the error '%v' should not have been returned", err)
		}
		if e != fileName {
			t.Errorf("the fileName should be '%s' not '%s'", e, fileName)
		}
	})
	t.Run("notGiven", func(t *testing.T) {
		e := "results.csv"
		os.Args = []string{"cmd", "www.google.com"}
		fileName, err := getArgFileName()
		if nil != err {
			t.Errorf("the error '%v' should not have been returned", err)
		}
		if e != fileName {
			t.Errorf("the fileName should be '%s' not '%s'", e, fileName)
		}
	})
	t.Run("empty", func(t *testing.T) {
		os.Args = []string{"cmd", "www.google.com", ""}
		_, err := getArgFileName()
		if nil == err {
			t.Errorf("an error should be returned because the file name is an empty string")
		}
	})
}

type mockLogger struct {
	msg string
}

func (l *mockLogger) Print(v ...interface{}) {
	switch i := v[0].(type) {
	case error:
		l.msg = i.Error()
	case string:
		l.msg = i
	}
}

func (l *mockLogger) Fatal(v ...interface{}) {
	switch i := v[0].(type) {
	case error:
		l.msg = i.Error()
	case string:
		l.msg = i
	}
}

func TestLogErr(t *testing.T) {
	t.Run("err=nil", func(t *testing.T) {
		l := new(mockLogger)
		logErr(l, nil)
		if "" != l.msg {
			t.Errorf("the error should have been an empty string not '%s'", l.msg)
		}
	})
	t.Run("err=test", func(t *testing.T) {
		e := errors.New("test")
		l := new(mockLogger)
		logErr(l, e)
		if e.Error() != l.msg {
			t.Errorf("the error should have been '%s' not '%s'", e.Error(), l.msg)
		}
	})
}

type dummyWriter struct {
	records [][]string
}

func newDummyWriter() *dummyWriter {
	w := new(dummyWriter)
	w.records = make([][]string, 0, 10)
	return w
}

func (w *dummyWriter) Write(record []string) error {
	w.records = append(w.records, record)
	return nil
}

func (w *dummyWriter) Flush() {}

func (w *dummyWriter) Error() error {
	return nil
}

func TestInitCSV(t *testing.T) {
	header := []string{"matrix", "degrees", "min degree", "max degree", "average degree", "vertices", "edges", "randic index", "distance matrix", "eccentricity", "min eccentricity", "max eccentricity", "average eccentricity", "average path length", "eigen values", "components"}
	w := newDummyWriter()
	err := initCSV(w)
	if nil != err {
		t.Errorf("the error '%v' should not have been returned", err)
	}
	w.Flush()
	if nil != w.Error() {
		t.Fatalf("the error '%v' should not have been encountered when flushing the writer's buffer", err)
	}
	if nil == w.records {
		t.Error("there should be values in the csv file")
	}
	if 1 != len(w.records) {
		t.Errorf("the length of the records read in should be 1 not '%d' because the header should be the only record", len(w.records))
	}
	record := w.records[0]
	if len(header) != len(record) {
		t.Errorf("the length of the header should be '%d' not '%d'", len(header), len(record))
	}
	for i := range header {
		if header[i] != record[i] {
			t.Errorf("the header value at index %d of the slice should be '%s' not '%s'", i, header[i], record[i])
		}
	}
}

func TestCloseWriter(t *testing.T) {
	w := newDummyWriter()
	err := closeWriter(w)
	if nil != err {
		t.Errorf("the error '%v' should not have been returned", err)
	}
}

func ExamplematString() {
	s := matString(mat.NewDense(2, 2, []float64{0, 1, 1, 0}), true)

	fmt.Print(s)

	// Output:
	// 0 1
	// 1 0
}

func TestMatString(t *testing.T) {
	t.Run("matrix=1x1", func(t *testing.T) {
		e := "0\n"
		m := mat.NewDense(1, 1, []float64{
			0,
		})
		s := matString(m, true)
		if e != s {
			t.Errorf("the string should be '%s' not '%s'", e, s)
		}
	})
	t.Run("matrix=1x5", func(t *testing.T) {
		e := "0 1 1 0 0\n"
		m := mat.NewDense(1, 5, []float64{
			0, 1, 1, 0, 0,
		})
		s := matString(m, true)
		if e != s {
			t.Errorf("the string should be '%s' not '%s'", e, s)
		}
	})
	t.Run("matrix=1x3", func(t *testing.T) {
		e := "0 1 1\n"
		m := mat.NewDense(1, 3, []float64{
			0, 1, 1,
		})
		s := matString(m, true)
		if e != s {
			t.Errorf("the string should be '%s' not '%s'", e, s)
		}
	})
	t.Run("matrix=4x3", func(t *testing.T) {
		e := "0 1 1\n0 0 0\n1 1 1\n1 0 1\n"
		m := mat.NewDense(4, 3, []float64{
			0, 1, 1,
			0, 0, 0,
			1, 1, 1,
			1, 0, 1,
		})
		s := matString(m, true)
		if e != s {
			t.Errorf("the string should be '%s' not '%s'", e, s)
		}
	})
	t.Run("matrix=3x4", func(t *testing.T) {
		e := "0 1 1 0\n0 0 0 0\n1 1 1 0\n"
		m := mat.NewDense(3, 4, []float64{
			0, 1, 1, 0,
			0, 0, 0, 0,
			1, 1, 1, 0,
		})
		s := matString(m, true)
		if e != s {
			t.Errorf("the string should be '%s' not '%s'", e, s)
		}
	})
	t.Run("matrix=4x4", func(t *testing.T) {
		e := "0 1\n1 0\n"
		m := mat.NewDense(2, 2, []float64{
			0, 1,
			1, 0,
		})
		s := matString(m, true)
		if e != s {
			t.Errorf("the string should be '%s' not '%s'", e, s)
		}
	})
}
