graphduck
===
graphduck generates binary, symmetric matrices with a zero diagnol. Given a length and an optional output file, graphduck will generate all possible matrices for that given length, determine their Randic Index, and output them to a csv file.

Getting Started
---
The following instructions detail how to use the program.

### Prerequisites
-   [go programming language](https://golang.org/doc/install)

### Installing
Retrieve the source files of the program.
```console
go get bitbucket.org/athrun22/graphduck
```

### Running
To use the program, a simple call to the script with the argument for length will suffice.
```console
$GOPATH/bin/graphduck 4
```
This will create a file titled "results.csv" within your current working directory.
A second argument can be supplied to write the output to a file with a different name or a different location. You will want to include the .csv file extension in the argument.
```console
$GOPATH/bin/graphduck 4 "$HOME/FourByFourMatrices.csv"
```

### Updating
To stay up-to-date, occasionally use the "go get" command to retrieve the latest source. The following command updates all packages.
```console
go get -u
```
The following command only updates graphduck.
```console
go get -u graphduck
```

Built With
---
- [gonum](https://www.gonum.org) - a set of packages designed to make writing numerical and scientific algorithms productive, performant, and scalable.

Versioning
---
[SemVer](http://semver.org/) is used for versioning. For the versions available, see the tags on this repository.

Contributing
---
Please read [CONTRIBUTING.md](./CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

Authors
---
-   Jordan Williams

License
---
This project is licensed under the MIT License - see the [LICENSE.md](./LICENSE.md) file for details
