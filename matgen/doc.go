// Package matgen is a package for generating matrices.
// Currently, there is support for binary, symmetric matrices with diagnols that only contain 0's.
package matgen
