package matgen

import "gonum.org/v1/gonum/mat"

// Generator represents an object for generating matrices.
type Generator interface {
	Generate() mat.Matrix
}
