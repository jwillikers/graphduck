package matgen

import (
	"fmt"
	"strings"
	"testing"

	"gonum.org/v1/gonum/mat"
)

// matString a matrix to a simple string representation.
func matString(m mat.Matrix) string {
	var s strings.Builder
	rows, cols := m.Dims()
	s.Grow(rows*cols*2 + 10)
	for r := 0; r < rows; r++ {
		for c := 0; c < cols-1; c++ {
			s.WriteString(fmt.Sprintf("% d ", int(m.At(r, c))))
		}
		s.WriteString(fmt.Sprintf("% d\n", int(m.At(r, cols-1))))
	}
	return s.String()
}

func ExampledetSize() {

	// determine the number of matrices given they are 3x3
	d := detSize(3)

	fmt.Print(d)

	// Output:
	// 8
}

func TestDetSize(t *testing.T) {
	t.Run("length=-1", func(t *testing.T) {
		e := 0
		c := detSize(-1)
		if e != c {
			t.Errorf("the number of combos should be %d not %d", e, c)
		}
	})
	t.Run("length=0", func(t *testing.T) {
		e := 0
		c := detSize(0)
		if e != c {
			t.Errorf("the number of combos should be %d not %d", e, c)
		}
	})
	t.Run("length=1", func(t *testing.T) {
		e := 1
		c := detSize(1)
		if e != c {
			t.Errorf("the number of combos should be %d not %d", e, c)
		}
	})
	t.Run("length=2", func(t *testing.T) {
		e := 2
		c := detSize(2)
		if e != c {
			t.Errorf("the number of combos should be %d not %d", e, c)
		}
	})
	t.Run("length=3", func(t *testing.T) {
		e := 8
		c := detSize(3)
		if e != c {
			t.Errorf("the number of combos should be %d not %d", e, c)
		}
	})
	t.Run("length=64", func(t *testing.T) {
		e := 64
		c := detSize(4)
		if e != c {
			t.Errorf("the number of combos should be %d not %d", e, c)
		}
	})
}

func ExampleGenerate() {

	// create a new generator for 2x2 matrices
	g := NewSymGen(2)

	// call generate until the generator is out of unique matrices
	for i := 0; i < g.Len(); i++ {
		fmt.Printf("%s\n", matString(g.Generate()))
	}

	// Output:
	// 0  0
	//  0  0
	//
	//  0  1
	//  1  0
}

func TestNewSymGen(t *testing.T) {
	t.Run("length=-1", func(t *testing.T) {
		g := NewSymGen(-1)
		if nil != g {
			t.Error("there should not be any generated matrices")
		}
	})
	t.Run("length=0", func(t *testing.T) {
		g := NewSymGen(0)
		if nil != g {
			t.Error("there should not be any generated matrices")
		}
	})
	t.Run("length=1", func(t *testing.T) {
		g := NewSymGen(1)
		if nil != g {
			t.Error("there should not be any generated matrices")
		}
	})
	t.Run("extra", func(t *testing.T) {
		g := NewSymGen(2)
		if nil == g {
			t.Error("there should be generated matrices")
		}
		if nil == g.Generate() {
			t.Error("the generated matrix should not be nil")
		}
		if nil == g.Generate() {
			t.Error("the generated matrix should not be nil")
		}
		if nil != g.Generate() {
			t.Error("the generated matrix should be nil")
		}
	})
	t.Run("length=2", func(t *testing.T) {
		e := []*mat.SymDense{
			mat.NewSymDense(2, []float64{
				0, 0,
				0, 0,
			}),
			mat.NewSymDense(2, []float64{
				0, 1,
				1, 0,
			}),
		}
		g := NewSymGen(2)
		if nil == g {
			t.Error("there should be generated matrices")
		}
		el := len(e)
		gl := g.Len()
		if el != gl {
			t.Errorf("there should be %d matrices generated not %d", el, gl)
		}
		for i := range e {
			gi := g.Generate()
			if !mat.Equal(e[i], gi) {
				t.Errorf("generated matrix %d should be %v not %v", i, e[i], gi)
			}
		}
	})
	t.Run("length=3", func(t *testing.T) {
		e := []*mat.SymDense{
			mat.NewSymDense(3, []float64{
				0, 0, 0,
				0, 0, 0,
				0, 0, 0,
			}),
			mat.NewSymDense(3, []float64{
				0, 1, 0,
				1, 0, 0,
				0, 0, 0,
			}),
			mat.NewSymDense(3, []float64{
				0, 0, 1,
				0, 0, 0,
				1, 0, 0,
			}),
			mat.NewSymDense(3, []float64{
				0, 1, 1,
				1, 0, 0,
				1, 0, 0,
			}),
			mat.NewSymDense(3, []float64{
				0, 0, 0,
				0, 0, 1,
				0, 1, 0,
			}),
			mat.NewSymDense(3, []float64{
				0, 1, 0,
				1, 0, 1,
				0, 1, 0,
			}),
			mat.NewSymDense(3, []float64{
				0, 0, 1,
				0, 0, 1,
				1, 1, 0,
			}),
			mat.NewSymDense(3, []float64{
				0, 1, 1,
				1, 0, 1,
				1, 1, 0,
			}),
		}
		g := NewSymGen(3)
		if nil == g {
			t.Error("there should be generated matrices")
		}
		el := len(e)
		gl := g.Len()
		if el != gl {
			t.Errorf("there should be %d matrices generated not %d", el, gl)
		}
		for i := range e {
			gi := g.Generate()
			if !mat.Equal(e[i], gi) {
				t.Errorf("generated matrix %d should be %v not %v", i, e[i], gi)
			}
		}
	})
	t.Run("length=4", func(t *testing.T) {
		e := []*mat.SymDense{
			mat.NewSymDense(4, []float64{
				0, 0, 0, 0,
				0, 0, 0, 0,
				0, 0, 0, 0,
				0, 0, 0, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 1, 0, 0,
				1, 0, 0, 0,
				0, 0, 0, 0,
				0, 0, 0, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 0, 1, 0,
				0, 0, 0, 0,
				1, 0, 0, 0,
				0, 0, 0, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 1, 1, 0,
				1, 0, 0, 0,
				1, 0, 0, 0,
				0, 0, 0, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 0, 0, 1,
				0, 0, 0, 0,
				0, 0, 0, 0,
				1, 0, 0, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 1, 0, 1,
				1, 0, 0, 0,
				0, 0, 0, 0,
				1, 0, 0, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 0, 1, 1,
				0, 0, 0, 0,
				1, 0, 0, 0,
				1, 0, 0, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 1, 1, 1,
				1, 0, 0, 0,
				1, 0, 0, 0,
				1, 0, 0, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 0, 0, 0,
				0, 0, 1, 0,
				0, 1, 0, 0,
				0, 0, 0, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 1, 0, 0,
				1, 0, 1, 0,
				0, 1, 0, 0,
				0, 0, 0, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 0, 1, 0,
				0, 0, 1, 0,
				1, 1, 0, 0,
				0, 0, 0, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 1, 1, 0,
				1, 0, 1, 0,
				1, 1, 0, 0,
				0, 0, 0, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 0, 0, 1,
				0, 0, 1, 0,
				0, 1, 0, 0,
				1, 0, 0, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 1, 0, 1,
				1, 0, 1, 0,
				0, 1, 0, 0,
				1, 0, 0, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 0, 1, 1,
				0, 0, 1, 0,
				1, 1, 0, 0,
				1, 0, 0, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 1, 1, 1,
				1, 0, 1, 0,
				1, 1, 0, 0,
				1, 0, 0, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 0, 0, 0,
				0, 0, 0, 1,
				0, 0, 0, 0,
				0, 1, 0, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 1, 0, 0,
				1, 0, 0, 1,
				0, 0, 0, 0,
				0, 1, 0, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 0, 1, 0,
				0, 0, 0, 1,
				1, 0, 0, 0,
				0, 1, 0, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 1, 1, 0,
				1, 0, 0, 1,
				1, 0, 0, 0,
				0, 1, 0, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 0, 0, 1,
				0, 0, 0, 1,
				0, 0, 0, 0,
				1, 1, 0, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 1, 0, 1,
				1, 0, 0, 1,
				0, 0, 0, 0,
				1, 1, 0, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 0, 1, 1,
				0, 0, 0, 1,
				1, 0, 0, 0,
				1, 1, 0, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 1, 1, 1,
				1, 0, 0, 1,
				1, 0, 0, 0,
				1, 1, 0, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 0, 0, 0,
				0, 0, 1, 1,
				0, 1, 0, 0,
				0, 1, 0, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 1, 0, 0,
				1, 0, 1, 1,
				0, 1, 0, 0,
				0, 1, 0, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 0, 1, 0,
				0, 0, 1, 1,
				1, 1, 0, 0,
				0, 1, 0, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 1, 1, 0,
				1, 0, 1, 1,
				1, 1, 0, 0,
				0, 1, 0, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 0, 0, 1,
				0, 0, 1, 1,
				0, 1, 0, 0,
				1, 1, 0, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 1, 0, 1,
				1, 0, 1, 1,
				0, 1, 0, 0,
				1, 1, 0, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 0, 1, 1,
				0, 0, 1, 1,
				1, 1, 0, 0,
				1, 1, 0, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 1, 1, 1,
				1, 0, 1, 1,
				1, 1, 0, 0,
				1, 1, 0, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 0, 0, 0,
				0, 0, 0, 0,
				0, 0, 0, 1,
				0, 0, 1, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 1, 0, 0,
				1, 0, 0, 0,
				0, 0, 0, 1,
				0, 0, 1, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 0, 1, 0,
				0, 0, 0, 0,
				1, 0, 0, 1,
				0, 0, 1, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 1, 1, 0,
				0, 0, 0, 0,
				1, 0, 0, 1,
				0, 0, 1, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 0, 0, 1,
				0, 0, 0, 0,
				0, 0, 0, 1,
				1, 0, 1, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 1, 0, 1,
				1, 0, 0, 0,
				0, 0, 0, 1,
				1, 0, 1, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 0, 1, 1,
				0, 0, 0, 0,
				1, 0, 0, 1,
				1, 0, 1, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 1, 1, 1,
				1, 0, 0, 0,
				1, 0, 0, 1,
				1, 0, 1, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 0, 0, 0,
				0, 0, 1, 0,
				0, 1, 0, 1,
				0, 0, 1, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 1, 0, 0,
				1, 0, 1, 0,
				0, 1, 0, 1,
				0, 0, 1, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 0, 1, 0,
				0, 0, 1, 0,
				1, 1, 0, 1,
				0, 0, 1, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 1, 1, 0,
				1, 0, 1, 0,
				1, 1, 0, 1,
				0, 0, 1, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 0, 0, 1,
				0, 0, 1, 0,
				0, 1, 0, 1,
				1, 0, 1, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 1, 0, 1,
				1, 0, 1, 0,
				0, 1, 0, 1,
				1, 0, 1, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 0, 1, 1,
				0, 0, 1, 0,
				1, 1, 0, 1,
				1, 0, 1, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 1, 1, 1,
				1, 0, 1, 0,
				1, 1, 0, 1,
				1, 0, 1, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 0, 0, 0,
				0, 0, 0, 1,
				0, 0, 0, 1,
				0, 1, 1, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 1, 0, 0,
				1, 0, 0, 1,
				0, 0, 0, 1,
				0, 1, 1, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 0, 1, 0,
				0, 0, 0, 1,
				1, 0, 0, 1,
				0, 1, 1, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 1, 1, 0,
				1, 0, 0, 1,
				1, 0, 0, 1,
				0, 1, 1, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 0, 0, 1,
				0, 0, 0, 1,
				0, 0, 0, 1,
				1, 1, 1, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 1, 0, 1,
				1, 0, 0, 1,
				0, 0, 0, 1,
				1, 1, 1, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 0, 1, 1,
				0, 0, 0, 1,
				1, 0, 0, 1,
				1, 1, 1, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 1, 1, 1,
				1, 0, 0, 1,
				1, 0, 0, 1,
				1, 1, 1, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 0, 0, 0,
				0, 0, 1, 1,
				0, 1, 0, 1,
				0, 1, 1, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 1, 0, 0,
				1, 0, 1, 1,
				0, 1, 0, 1,
				0, 1, 1, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 0, 1, 0,
				0, 0, 1, 1,
				1, 1, 0, 1,
				0, 1, 1, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 1, 1, 0,
				1, 0, 1, 1,
				1, 1, 0, 1,
				0, 1, 1, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 0, 0, 1,
				0, 0, 1, 1,
				0, 1, 0, 1,
				1, 1, 1, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 1, 0, 1,
				1, 0, 1, 1,
				0, 1, 0, 1,
				1, 1, 1, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 0, 1, 1,
				0, 0, 1, 1,
				1, 1, 0, 1,
				1, 1, 1, 0,
			}),
			mat.NewSymDense(4, []float64{
				0, 1, 1, 1,
				1, 0, 1, 1,
				1, 1, 0, 1,
				1, 1, 1, 0,
			}),
		}
		g := NewSymGen(4)
		if nil == g {
			t.Error("there should be generated matrices")
		}
		el := len(e)
		gl := g.Len()
		if el != gl {
			t.Errorf("there should be %d matrices generated not %d", el, gl)
		}
		for i := range e {
			gi := g.Generate()
			if !mat.Equal(e[i], gi) {
				t.Errorf("generated matrix %d should be %v not %v", i, e[i], gi)
			}
		}
	})
}

func BenchmarkGen(b *testing.B) {
	b.Run("length=2", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			NewSymGen(2)
		}
	})
	b.Run("length=3", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			NewSymGen(3)
		}
	})
	b.Run("length=4", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			NewSymGen(4)
		}
	})
	b.Run("length=5", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			NewSymGen(5)
		}
	})
	b.Run("length=6", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			NewSymGen(6)
		}
	})
}
