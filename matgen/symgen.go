package matgen

import "gonum.org/v1/gonum/mat"

// SymGen generates symmetric, binary matrices of a pre-defined length.
type SymGen struct {
	length int
	count  int
	index  int
	prev   *mat.SymDense
}

// NewSymGen creates and initializes a SymGen for generating length x length symmetric matrices.
func NewSymGen(length int) *SymGen {
	if length <= 1 {
		return nil
	}
	return &SymGen{length, detSize(length), 0, mat.NewSymDense(length, nil)}
}

// Len returns the number of unique matrices of the given size that can be generated.
func (g *SymGen) Len() int {
	return g.count
}

// Generate creates the next logical symmetric matrix.
// It accomplishes this by acting as if the matrix represents a binary number and adding 1 to that binary number, changing 1's to 0's until it comes to a 0, which it then turns into a 1.
func (g *SymGen) Generate() *mat.SymDense {
	if g.index >= g.count {
		return nil
	}

	cp := mat.NewSymDense(g.length, nil)

	// don't modify prev if this is the first matrix
	if g.index > 0 {
		var r int
		colStart := 1
	outer:
		for r = 0; r < g.length-1; r++ {
			for c := colStart; c < g.length; c++ {
				if g.prev.At(r, c) == 1 {
					g.prev.SetSym(r, c, 0)
				} else {
					g.prev.SetSym(r, c, 1)
					break outer
				}
			}
			// the first column checked moves to the right by one every time the row increases
			colStart++
		}
	}
	g.index++
	cp.CopySym(g.prev)
	return cp
}

// detSize determines the number of unique symmetric matrices of the given length that exist.
// The number of matrices is the number of possible combinations. Because this is a binary matrix, that makes it 2 to the power of changing elements.
// For a 3x3 matrix, the number of combinations is 2 ^ 3, or 8, as seen below.
// 1 * 2 * 2 *
// 1 * 1 * 2 *
// 1 * 1 * 1 *
func detSize(length int) int {
	if length < 1 {
		return 0
	}
	if length == 1 {
		return 1
	}
	// half of the elements in the matrix minus half of the elements in the diagnol
	numElements := (length - 1) * length / 2
	size := 2
	for i := 1; i < numElements; i++ {
		size *= 2
	}
	return size
}
