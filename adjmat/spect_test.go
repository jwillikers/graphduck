package adjmat

import (
	"fmt"
	"testing"

	"gonum.org/v1/gonum/mat"
)

func ExampleSpect_Components() {

	// the spectrum of the matrix, i.e. the eigen values
	s := NewSpect(mat.NewVecDense(4, []float64{-1.618033988749895, -0.6180339887498951, 0.6180339887498945, 1.6180339887498958}))

	c := s.Components()

	fmt.Printf("there are %d components for the matrix", c)

	// Output:
	// there are 2 components for the matrix
}

func TestComponents(t *testing.T) {
	e := 2

	s := NewSpect(mat.NewVecDense(4, []float64{-1.618033988749895, -0.6180339887498951, 0.6180339887498945, 1.6180339887498958}))

	c := s.Components()

	if e != c {
		t.Errorf("there should be %d components for the matrix not %d", e, c)
	}
}

func BenchmarkCalcComponents(b *testing.B) {
	s := NewSpect(mat.NewVecDense(4, []float64{-1.618033988749895, -0.6180339887498951, 0.6180339887498945, 1.6180339887498958}))

	for i := 0; i < b.N; i++ {
		s.calcComponents()
	}
}
