package adjmat

import "gonum.org/v1/gonum/mat"

// Distance represents a distance matrix.
type Distance interface {
	AveragePathLength() (int, int)
	Eccentricity() mat.Vector
	String() string
}
