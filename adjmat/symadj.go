package adjmat

import (
	"fmt"
	"math"
	"strings"

	"gonum.org/v1/gonum/mat"
)

// SymAdj represents a symmetric adjacency matrix.
type SymAdj struct {
	m        mat.Symmetric
	deg      mat.Vector
	dist     Distance
	edges    int
	lap      mat.Symmetric
	cRandic  bool
	randic   float64
	spect    Spectrum
	vertices int
}

// NewSymAdj initializes a symmetric adjacency matrix to the given symmetric matrix.
func NewSymAdj(m mat.Symmetric) *SymAdj {
	a := new(SymAdj)
	a.m = m
	a.edges = int(mat.Sum(a.m)) / 2
	a.vertices = m.Symmetric()
	return a
}

// calcDegrees calculates the degree of each vertex of the graph.
func (a *SymAdj) calcDegrees() {
	rows, cols := a.m.Dims()
	deg := mat.NewVecDense(cols, nil)
	for c := 0; c < cols; c++ {
		for r := 0; r < rows; r++ {
			deg.SetVec(c, deg.AtVec(c)+a.m.At(r, c))
		}
	}
	a.deg = deg
}

// Degrees is the vector containing the degree for each vertex of the graph.
func (a *SymAdj) Degrees() mat.Vector {
	if a.deg == nil {
		a.calcDegrees()
	}
	return a.deg
}

// calcDistanceMatrix finds the distance matrix.
// Negative one represents infinity in the distance matrix.
func (a *SymAdj) calcDistanceMatrix() {
	rows, cols := a.m.Dims()
	dm := mat.NewDense(rows, cols, nil)
	powers := powers(a.m, 0, rows)
	for r := 0; r < rows; r++ {
		for c := 0; c < cols; c++ {
			if r == c {
				continue
			}
			for p := 0; p < rows && dm.At(r, c) == 0; p++ {
				if powers[p].At(r, c) > 0 {
					dm.Set(r, c, float64(p))
				} else if p == rows-1 {
					dm.Set(r, c, float64(-1))
				}
			}
		}
	}
	a.dist = NewDist(dm)
}

// DistanceMatrix returns the distance matrix.
// Negative one represents infinity in the distance matrix.
func (a *SymAdj) DistanceMatrix() Distance {
	if a.dist == nil {
		a.calcDistanceMatrix()
	}
	return a.dist
}

// Edges returns the number of edges in the graph.
func (a *SymAdj) Edges() int {
	return a.edges
}

// calcRandicIndex determines the Randic index from the adjacency matrix and vertex degrees vector.
func (a *SymAdj) calcRandicIndex() {
	randicIndex := float64(0)
	deg := a.Degrees()
	rows, _ := a.m.Dims()
	for r := 0; r < rows; r++ {
		for c := 0; c < r; c++ {
			if a.m.At(r, c) == 1 {
				randicIndex += 1 / math.Sqrt(deg.AtVec(r)*deg.AtVec(c))
			}
		}
	}
	a.randic = randicIndex
	a.cRandic = true
}

// RandicIndex returns the Randic index of the graph.
func (a *SymAdj) RandicIndex() float64 {
	if !a.cRandic {
		a.calcRandicIndex()
	}
	return a.randic
}

// calcLaplacianMatrix generates the Laplacian matrix by taking the 0 matrix with a diagnol of the degree vector and subtracting the adjacency matrix from it.
func (a *SymAdj) calcLaplacianMatrix() {
	lapSlice := make([]float64, a.vertices*a.vertices)

	// use the degree vector as the diagnol of the Laplacian matrix
	for i := 0; i < a.vertices; i++ {
		lapSlice[i*(a.vertices+1)] = a.deg.AtVec(i)
	}

	lap := mat.NewDense(a.vertices, a.vertices, lapSlice)

	// subtract the diagnol degree matrix (made above) from the adjacency matrix
	lap.Sub(lap, a.m)

	a.lap = mat.NewSymDense(a.vertices, lapSlice)
}

// LaplacianMatrix retrieves the laplacian matrix of this adjacency matrix.
func (a *SymAdj) LaplacianMatrix() mat.Symmetric {
	if a.lap == nil {
		if a.deg == nil {
			a.calcDegrees()
		}
		a.calcLaplacianMatrix()
	}
	return a.lap
}

// calcSpectrum calculates the eigen values of the matrix, i.e. the spectrum of the graph.
func (a *SymAdj) calcSpectrum() {
	e := new(mat.EigenSym)
	e.Factorize(a.LaplacianMatrix(), false)
	v := e.Values(nil)
	a.spect = NewSpect(mat.NewVecDense(len(v), v))
}

// Spectrum returns the spectrum of the graph.
func (a *SymAdj) Spectrum() Spectrum {
	if a.spect == nil {
		if a.deg == nil {
			a.calcDegrees()
		}
		if a.lap == nil {
			a.calcLaplacianMatrix()
		}
		a.calcSpectrum()
	}
	return a.spect
}

// Vertices returns the number of vertices in the graph.
func (a *SymAdj) Vertices() int {
	return a.vertices
}

func (a *SymAdj) String() string {
	var s strings.Builder
	rows, cols := a.m.Dims()
	s.Grow(rows*cols*2 + 10)
	for r := 0; r < rows; r++ {
		for c := 0; c < cols-1; c++ {
			s.WriteString(fmt.Sprintf("% d ", int(a.m.At(r, c))))
		}
		s.WriteString(fmt.Sprintf("% d\n", int(a.m.At(r, cols-1))))
	}
	return s.String()
}
