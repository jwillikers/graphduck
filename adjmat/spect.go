package adjmat

import (
	"fmt"
	"strings"

	"gonum.org/v1/gonum/mat"
)

// Spect represents a matrix's spectrum.
type Spect struct {
	v mat.Vector
	c int
}

// NewSpect creates a spectrum backed by the given matrix.
func NewSpect(v mat.Vector) *Spect {
	return &Spect{v, -1}
}

// calcComponents determines the number of components in the graph.
func (s *Spect) calcComponents() {
	c := 0
	for i := 0; i < s.v.Len(); i++ {
		if s.v.AtVec(i) < 1.0e-10 {
			c++
		}
	}
	s.c = c
}

// Components returns the number of components in the graph.
func (s *Spect) Components() int {
	if s.c < 0 {
		s.calcComponents()
	}
	return s.c
}

func (s *Spect) String() string {
	var b strings.Builder
	rows := s.v.Len()
	b.Grow(rows * 13)
	for r := 0; r < rows; r++ {
		b.WriteString(fmt.Sprintf("% .9f\n", s.v.AtVec(r)))
	}
	return b.String()
}
