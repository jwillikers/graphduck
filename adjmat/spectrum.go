package adjmat

// Spectrum is a graph's spectrum.
type Spectrum interface {
	Components() int
	String() string
}
