// Package adjmat is for handling adjacency matrix representations of graphs for calculating different values.
// It is structured based on the requirements and dependencies of calculations and formulas and less so on the data structures involved.
package adjmat
