package adjmat

import (
	"fmt"
	"testing"

	"gonum.org/v1/gonum/mat"
)

func Examplepowers() {

	// create a matrix
	m := mat.NewDense(4, 4, []float64{
		1, 0, 0, 0,
		0, 2, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1,
	})

	// calculate the matrix to first, second, and third powers
	ps := powers(m, 1, 4)

	fmt.Printf("m ^ 1 = %v\nm ^ 2 = %v\nm ^ 3 = %v\n", ps[0], ps[1], ps[2])

	// Output:
	// m ^ 1 = &{{4 4 4 [1 0 0 0 0 2 0 0 0 0 1 0 0 0 0 1]} 4 4}
	// m ^ 2 = &{{4 4 4 [1 0 0 0 0 4 0 0 0 0 1 0 0 0 0 1]} 4 4}
	// m ^ 3 = &{{4 4 4 [1 0 0 0 0 8 0 0 0 0 1 0 0 0 0 1]} 4 4}
}

func TestPowers(t *testing.T) {
	m := mat.NewDense(4, 4, []float64{
		1, 0, 0, 0,
		0, 2, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1,
	})
	e := []*mat.Dense{
		mat.NewDense(4, 4, []float64{
			1, 0, 0, 0,
			0, 2, 0, 0,
			0, 0, 1, 0,
			0, 0, 0, 1,
		}),
		mat.NewDense(4, 4, []float64{
			1, 0, 0, 0,
			0, 4, 0, 0,
			0, 0, 1, 0,
			0, 0, 0, 1,
		}),
		mat.NewDense(4, 4, []float64{
			1, 0, 0, 0,
			0, 8, 0, 0,
			0, 0, 1, 0,
			0, 0, 0, 1,
		}),
	}
	ps := powers(m, 1, 4)
	if nil == ps {
		t.Error("there should be power matrices generated not nil")
	}
	if len(e) != len(ps) {
		t.Errorf("there should be %d power matrices not %d", len(e), len(ps))
	}
	for i := range e {
		if !mat.Equal(e[i], ps[i]) {
			t.Errorf("the power matrix should be %v not %v", e[i], ps[i])
		}
	}
}

func BenchmarkPowers(b *testing.B) {
	m := mat.NewDense(4, 4, []float64{
		1, 2, 3, 4,
		8, 7, 6, 5,
		9, 10, 11, 12,
		16, 15, 14, 13,
	})

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		powers(m, 0, 3)
	}
}
