package adjmat

import (
	"gonum.org/v1/gonum/mat"
)

// powers calculates the power of the matrix to each exponent in the range from begin inclusive to end exclusive.
// The power matrices are returned in ascending order.
func powers(m mat.Matrix, begin, end int) []mat.Matrix {
	ms := make([]mat.Matrix, end-begin)
	rows, cols := m.Dims()
	e := begin
	for i := range ms {
		ms[i] = mat.NewDense(rows, cols, nil)
		d, _ := ms[i].(*mat.Dense)
		d.Pow(m, e)
		e++
	}
	return ms
}
