package adjmat

import (
	"fmt"
	"testing"

	"gonum.org/v1/gonum/mat"
)

func ExampleDist_AveragePathLength() {

	// create the distance matrix
	d := NewDist(mat.NewDense(4, 4, []float64{
		0, 0, 1, 1,
		0, 0, 0, 1,
		1, 0, 0, 0,
		1, 1, 0, 0,
	}))

	// calculate the average path length for the vertices in the graph
	l, p := d.AveragePathLength()

	fmt.Printf("l = %d\np = %d\n", l, p)

	// Output:
	// l = 6
	// p = 12
}

func TestAveragePathLength(t *testing.T) {
	ep := 12
	el := 6

	d := NewDist(mat.NewDense(4, 4, []float64{
		0, 0, 1, 1,
		0, 0, 0, 1,
		1, 0, 0, 0,
		1, 1, 0, 0,
	}))

	l, p := d.AveragePathLength()

	if el != l {
		t.Errorf("l should be %d not %d", el, l)
	}
	if ep != p {
		t.Errorf("p should be %d not %d", ep, p)
	}
}

func BenchmarkCalcAveragePathLength(b *testing.B) {
	d := NewDist(mat.NewDense(4, 4, []float64{
		0, -1, 2, 2,
		-1, 0, -1, 2,
		2, -1, 0, -1,
		2, 2, -1, 0,
	}))

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		d.calcAveragePathLength()
	}
}

func ExampleDist_Eccentricity() {

	// the distance matrix
	d := NewDist(mat.NewDense(4, 4, []float64{
		0, -1, 2, 2,
		-1, 0, -1, 2,
		2, -1, 0, -1,
		2, 2, -1, 0,
	}))

	// calculate the eccentricity using the distance matrix
	e := d.Eccentricity()

	fmt.Printf("eccentricity of the matrix is %v", e)

	// Output:
	// eccentricity of the matrix is &{{1 [3 0 0 3]} 4}
}

func TestEccentricity(t *testing.T) {
	e := mat.NewVecDense(4, []float64{
		3, 0, 0, 3,
	})

	d := NewDist(mat.NewDense(4, 4, []float64{
		0, -1, 2, 2,
		-1, 0, -1, 2,
		2, -1, 0, -1,
		2, 2, -1, 0,
	}))

	ecc := d.Eccentricity()
	if nil == ecc {
		t.Error("the eccentricity of the matrix should not be nil")
	}
	if !mat.Equal(e, ecc) {
		t.Errorf("the eccentricity should be %v not %v", e, ecc)
	}
}

func BenchmarkCalcEccentricity(b *testing.B) {
	d := NewDist(mat.NewDense(4, 4, []float64{
		0, -1, 2, 2,
		-1, 0, -1, 2,
		2, -1, 0, -1,
		2, 2, -1, 0,
	}))

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		d.calcEccentricity()
	}
}
