package adjmat

import (
	"fmt"
	"strings"

	"gonum.org/v1/gonum/mat"
)

// Dist is a distance matrix.
type Dist struct {
	m   mat.Matrix
	l   int
	p   int
	ecc mat.Vector
}

// NewDist initializes an n x n distance matrix.
func NewDist(m mat.Matrix) *Dist {
	return &Dist{m, -1, -1, nil}
}

// calcAveragePathLength determines the average path length from the distance matrix.
func (d *Dist) calcAveragePathLength() {
	rows, cols := d.m.Dims()
	p := rows * (rows - 1)
	var l int
	for r := 0; r < rows; r++ {
		for c := 0; c < cols; c++ {
			if d.m.At(r, c) > 0 {
				l += int(d.m.At(r, c))
			}
		}
	}
	d.l = l
	d.p = p
}

// AveragePathLength returns the average path length from the distance matrix in terms of l and p.
func (d *Dist) AveragePathLength() (int, int) {
	if d.l < 0 {
		d.calcAveragePathLength()
	}
	return d.l, d.p
}

// calcEccentricity determines the eccentricity of each vertex in the graph.
func (d *Dist) calcEccentricity() {
	rows, cols := d.m.Dims()
	e := mat.NewVecDense(rows, nil)
	for r := 0; r < rows; r++ {
		s := float64(0)
		for c := 0; c < cols; c++ {
			s += d.m.At(r, c)
		}
		e.SetVec(r, s)
	}
	d.ecc = e
}

// Eccentricity returns the eccentricity of each vertex in the graph.
func (d *Dist) Eccentricity() mat.Vector {
	if d.ecc == nil {
		d.calcEccentricity()
	}
	return d.ecc
}

func (d *Dist) String() string {
	var s strings.Builder
	rows, cols := d.m.Dims()
	s.Grow(rows*cols*2 + 10)
	for r := 0; r < rows; r++ {
		for c := 0; c < cols-1; c++ {
			s.WriteString(fmt.Sprintf("% d ", int(d.m.At(r, c))))
		}
		s.WriteString(fmt.Sprintf("% d\n", int(d.m.At(r, cols-1))))
	}
	return s.String()
}
