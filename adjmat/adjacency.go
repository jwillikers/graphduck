package adjmat

import "gonum.org/v1/gonum/mat"

// Adjacency represents an adjacency matrix.
type Adjacency interface {
	Degrees() mat.Vector
	DistanceMatrix() Distance
	Edges() int
	RandicIndex() float64
	Spectrum() Spectrum
	String() string
	Vertices() int
}
