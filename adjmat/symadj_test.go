package adjmat

import (
	"fmt"
	"math"
	"strconv"
	"testing"

	"gonum.org/v1/gonum/mat"
)

func ExampleSymAdj_Degrees() {

	// create an adjacency matrix
	a := NewSymAdj(mat.NewSymDense(4, []float64{
		0, 1, 2, 3,
		1, 0, 1, 2,
		2, 1, 0, 1,
		3, 2, 1, 0,
	}))

	// calculate the degrees of the graph
	s := a.Degrees()

	fmt.Printf("the degrees of the graph are %v", s)

	// Output:
	// the degrees of the graph are &{{1 [6 4 4 6]} 4}
}

func TestDegrees(t *testing.T) {
	a := NewSymAdj(mat.NewSymDense(4, []float64{
		0, 1, 2, 3,
		1, 0, 1, 2,
		2, 1, 0, 1,
		3, 2, 1, 0,
	}))
	e := mat.NewVecDense(4, []float64{6, 4, 4, 6})
	s := a.Degrees()
	if nil == s {
		t.Error("the sum of the columns should not be nil")
	}
	el := e.Len()
	sl := s.Len()
	if el != sl {
		t.Errorf("the length of the vector should be %d not %d", el, sl)
	}
	if !mat.Equal(e, s) {
		t.Errorf("the vector should be %v not %v", e, s)
	}
}

func BenchmarkCalcDegrees(b *testing.B) {
	a := NewSymAdj(mat.NewSymDense(4, []float64{
		0, 1, 2, 3,
		1, 0, 1, 2,
		2, 1, 0, 1,
		3, 2, 1, 0,
	}))

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		a.calcDegrees()
	}
}

func ExampleSymAdj_Edges() {

	// create the symmetric adjacency matrix
	a := NewSymAdj(mat.NewSymDense(4, []float64{
		0, 0, 0, 1,
		0, 0, 0, 1,
		0, 0, 0, 1,
		1, 1, 1, 0,
	}))

	// calculate the number of edges in the graph
	e := a.Edges()

	fmt.Printf("there are %d edges in the matrix", e)

	// Output:
	// there are 3 edges in the matrix
}

func TestEdges(t *testing.T) {
	t.Run("length=2", func(t *testing.T) {
		exp := 1
		a := NewSymAdj(mat.NewSymDense(2, []float64{
			0, 1,
			1, 0,
		}))
		edg := a.Edges()
		if exp != edg {
			t.Errorf("the number of edges should be %d not %d", exp, edg)
		}
	})
	t.Run("length=4", func(t *testing.T) {
		exp := 3
		a := NewSymAdj(mat.NewSymDense(4, []float64{
			0, 0, 0, 1,
			0, 0, 0, 1,
			0, 0, 0, 1,
			1, 1, 1, 0,
		}))
		edg := a.Edges()
		if exp != edg {
			t.Errorf("the number of edges should be %d not %d", exp, edg)
		}
	})
}

func ExampleSymAdj_DistanceMatrix() {

	// create the symmetric adjacency matrix
	a := NewSymAdj(mat.NewSymDense(4, []float64{
		0, 0, 0, 1,
		0, 0, 0, 0,
		0, 0, 0, 0,
		1, 0, 0, 0,
	}))

	// calculate the distance matrix for the graph
	dm := a.DistanceMatrix()

	fmt.Printf("Distance matrix:\n%s\n", dm)

	// Output:
	// Distance matrix:
	//  0 -1 -1  1
	// -1  0 -1 -1
	// -1 -1  0 -1
	//  1 -1 -1  0
}

func TestDistanceMatrix(t *testing.T) {
	e := mat.NewDense(4, 4, []float64{
		0, -1, -1, 1,
		-1, 0, -1, -1,
		-1, -1, 0, -1,
		1, -1, -1, 0,
	})
	a := NewSymAdj(mat.NewSymDense(4, []float64{
		0, 0, 0, 1,
		0, 0, 0, 0,
		0, 0, 0, 0,
		1, 0, 0, 0,
	}))
	dm := a.DistanceMatrix()
	if nil == dm {
		t.Error("the distance matrix should not be nil")
	}
	d, ok := dm.(*Dist)
	if !ok {
		t.Errorf("the distance matrix %v should be of type dist", dm)
	}
	if !mat.Equal(e, d.m) {
		t.Errorf("the distance matrix should be %v not %v", e, dm)
	}
}

func BenchmarkCalcDistanceMatrix(b *testing.B) {
	a := NewSymAdj(mat.NewSymDense(4, []float64{
		0, 1, 0, 1,
		1, 0, 1, 0,
		0, 1, 0, 0,
		1, 0, 0, 0,
	}))

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		a.calcDistanceMatrix()
	}
}

func ExampleSymAdj_RandicIndex() {

	// create a symmetric adjacency matrix
	a := NewSymAdj(mat.NewSymDense(4, []float64{
		0, 1, 0, 0,
		1, 0, 1, 0,
		0, 1, 0, 1,
		0, 0, 1, 0,
	}))

	// calculate the RandicIndex
	ri := a.RandicIndex()

	fmt.Printf("the randic index of the matrix is %s", strconv.FormatFloat(ri, 'f', 16, 64))

	// Output:
	// the randic index of the matrix is 1.9142135623730949
}

func TestRandicIndex(t *testing.T) {
	e := math.Sqrt(2) + float64(0.5)
	a := NewSymAdj(mat.NewSymDense(4, []float64{
		0, 1, 0, 0,
		1, 0, 1, 0,
		0, 1, 0, 1,
		0, 0, 1, 0,
	}))
	ri := a.RandicIndex()
	if math.Round(e*10000) != math.Round(ri*10000) {
		t.Errorf("the randic index should be %f not %f", e, ri)
	}
}

func BenchmarkCalcRandicIndex(b *testing.B) {
	a := NewSymAdj(mat.NewSymDense(4, []float64{
		0, 1, 0, 1,
		1, 0, 1, 0,
		0, 1, 0, 0,
		1, 0, 0, 0,
	}))

	// the degrees are required, so calculate them before timing
	a.Degrees()

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		a.calcRandicIndex()
	}
}

func ExampleSymAdj_LaplacianMatrix() {

	// create the symmetric adjacency matrix
	a := NewSymAdj(mat.NewSymDense(4, []float64{
		0, 0, 0, 1,
		0, 0, 0, 0,
		0, 0, 0, 0,
		1, 0, 0, 0,
	}))

	// calculate the laplacian matrix of the graph
	l := a.LaplacianMatrix()

	fmt.Printf("the laplacian matrix of the graph is: %v", l)

	// Output:
	// the laplacian matrix of the graph is: &{{4 4 [1 0 0 -1 0 0 0 0 0 0 0 0 -1 0 0 1] 121} 4}
}

func TestLaplacianMatrix(t *testing.T) {
	e := mat.NewSymDense(4, []float64{
		1, 0, 0, -1,
		0, 0, 0, 0,
		0, 0, 0, 0,
		-1, 0, 0, 1,
	})
	a := NewSymAdj(mat.NewSymDense(4, []float64{
		0, 0, 0, 1,
		0, 0, 0, 0,
		0, 0, 0, 0,
		1, 0, 0, 0,
	}))

	l := a.LaplacianMatrix()

	if nil == l {
		t.Error("the laplacian matrix of the graph should not be nil")
	}
	if !mat.Equal(e, l) {
		t.Errorf("the laplacian matrix should be %v not %v", e, l)
	}
}

func ExampleSymAdj_Spectrum() {

	// create the symmetric adjacency matrix
	a := NewSymAdj(mat.NewSymDense(4, []float64{
		0, 0, 0, 1,
		0, 0, 0, 0,
		0, 0, 0, 0,
		1, 0, 0, 0,
	}))

	// calculate the spectrum of the graph
	s := a.Spectrum()

	fmt.Printf("the spectrum of the graph is:\n%s", s)

	// Output:
	// the spectrum of the graph is:
	//  0.0000000000
	//  0.0000000000
	//  0.0000000000
	//  2.0000000000
}

func TestSpectrum(t *testing.T) {
	e := mat.NewVecDense(4, []float64{
		0, 0, 0, 2,
	})
	a := NewSymAdj(mat.NewSymDense(4, []float64{
		0, 0, 0, 1,
		0, 0, 0, 0,
		0, 0, 0, 0,
		1, 0, 0, 0,
	}))
	s := a.Spectrum()
	if nil == s {
		t.Error("the spectrum of the graph should not be nil")
	}
	spect, ok := s.(*Spect)
	if !ok {
		t.Errorf("the returned value %v from Spectrum() should be of the type spect", s)
	}
	if !mat.Equal(e, spect.v) {
		t.Errorf("the spectrum should be %v not %v", e, spect.v)
	}
}

func BenchmarkCalcSpectrum(b *testing.B) {
	a := NewSymAdj(mat.NewSymDense(4, []float64{
		0, 1, 0, 1,
		1, 0, 1, 0,
		0, 1, 0, 0,
		1, 0, 0, 0,
	}))

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		a.calcSpectrum()
	}
}

func ExampleSymAdj_Vertices() {

	// create the symmetric adjacency matrix
	a := NewSymAdj(mat.NewSymDense(4, []float64{
		0, 0, 0, 1,
		0, 0, 0, 0,
		0, 0, 0, 0,
		1, 0, 0, 0,
	}))

	// calculate the number of vertices in the graph
	v := a.Vertices()

	fmt.Printf("the graph has %d vertices", v)

	// Output:
	// the graph has 4 vertices
}

func TestVertices(t *testing.T) {
	e := 4
	a := NewSymAdj(mat.NewSymDense(4, []float64{
		0, 0, 0, 1,
		0, 0, 0, 0,
		0, 0, 0, 0,
		1, 0, 0, 0,
	}))
	v := a.Vertices()
	if e != v {
		t.Errorf("there should be %d vertices in the graph not %d", e, v)
	}
}

func TestString(t *testing.T) {
	t.Run("binary", func(t *testing.T) {
		e := " 0  0  0  1\n 0  0  0  0\n 0  0  0  0\n 1  0  0  0\n"
		a := NewSymAdj(mat.NewSymDense(4, []float64{
			0, 0, 0, 1,
			0, 0, 0, 0,
			0, 0, 0, 0,
			1, 0, 0, 0,
		}))
		if e != a.String() {
			t.Errorf("the string form of the matrix should be:\n%snot:\n%s", e, a)
		}
	})
	t.Run("negative", func(t *testing.T) {
		e := "-1  0  0  1\n 0  0  0  0\n 0  0  0  0\n 1  0  0 -1\n"
		a := NewSymAdj(mat.NewSymDense(4, []float64{
			-1, 0, 0, 1,
			0, 0, 0, 0,
			0, 0, 0, 0,
			1, 0, 0, -1,
		}))
		if e != a.String() {
			t.Errorf("the string form of the matrix should be:\n%snot:\n%s", e, a)
		}
	})
	t.Run("doubledigits", func(t *testing.T) {
		e := " 1  10  0  1\n 10  0  0  0\n 0  0  0  0\n 1  0  0  1\n"
		a := NewSymAdj(mat.NewSymDense(4, []float64{
			1, 10, 0, 1,
			10, 0, 0, 0,
			0, 0, 0, 0,
			1, 0, 0, 1,
		}))
		if e != a.String() {
			t.Errorf("the string form of the matrix should be:\n%snot:\n%s", e, a)
		}
	})
}
