package main

import (
	"encoding/csv"
	"errors"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"

	"bitbucket.org/athrun22/graphduck/adjmat"
	"bitbucket.org/athrun22/graphduck/matgen"
	"gonum.org/v1/gonum/mat"
)

// getArgLength retrieves the argument denoting the length of the matrix.
func getArgLength() (int, error) {
	if len(os.Args) < 2 {
		return 0, errors.New("a matrix length must be provided")
	}

	l, err := strconv.Atoi(os.Args[1])
	if err == nil && l <= 1 {
		return l, errors.New("the matrix length must exceed one")
	}
	return l, err
}

// getArgFileName retrieves the command line argument that is file name for the output file.
// The file "results.csv" is used as the default file if none is given.
func getArgFileName() (string, error) {
	var filename string
	var err error
	if len(os.Args) > 2 {
		if os.Args[2] == "" {
			err = errors.New("the file's name cannot be an empty string")
		} else {
			filename = os.Args[2]
		}
	} else {
		filename = "results.csv"
	}
	return filename, err
}

// logErr logs an error, if any, and quits the program.
func logErr(l logger, err error) {
	if err == nil {
		return
	}
	l.Fatal(err)
}

// initCSV writes out the csv file's header.
func initCSV(w writer) error {
	header := []string{"matrix", "degrees", "min degree", "max degree", "average degree", "vertices", "edges", "randic index", "distance matrix", "eccentricity", "min eccentricity", "max eccentricity", "average eccentricity", "average path length", "eigen values", "components"}
	return w.Write(header)
}

// closeWriter closes a writer by flushing any buffered data to the destination.
func closeWriter(w writer) error {
	w.Flush()
	return w.Error()
}

// matString converts a matrix to a simple string representation.
// If true is given as the integer argument, the elements in the matrix are output as integers, otherwise they are floats with ten digits after the decimal.
func matString(m mat.Matrix, integer bool) string {
	var s strings.Builder
	prec := 10
	if integer {
		prec = 0
	}
	rows, cols := m.Dims()
	s.Grow(rows * cols * 13)
	for r := 0; r < rows; r++ {
		for c := 0; c < cols-1; c++ {
			s.WriteString(strconv.FormatFloat(m.At(r, c), 'f', prec, 64))
			s.WriteRune(' ')
		}
		s.WriteString(strconv.FormatFloat(m.At(r, cols-1), 'f', prec, 64))
		s.WriteRune('\n')
	}
	return s.String()
}

// adjCSVRecord converts the Adjacency object into a string slice of all its pertinent data.
func adjCSVRecord(a adjmat.Adjacency) []string {
	deg := a.Degrees()
	ri := a.RandicIndex()
	dist := a.DistanceMatrix()
	p, l := dist.AveragePathLength()
	ecc := dist.Eccentricity()
	spect := a.Spectrum()
	vertices := a.Vertices()
	return []string{
		a.String(),
		matString(deg, true),
		strconv.FormatFloat(mat.Min(deg), 'f', 0, 64),
		strconv.FormatFloat(mat.Max(deg), 'f', 0, 64),
		strconv.FormatFloat(mat.Sum(deg)/float64(vertices), 'f', 3, 64),
		strconv.Itoa(vertices),
		strconv.Itoa(a.Edges()),
		strconv.FormatFloat(ri, 'f', 10, 64),
		dist.String(),
		matString(ecc, true),
		strconv.FormatFloat(mat.Min(ecc), 'f', 0, 64),
		strconv.FormatFloat(mat.Max(ecc), 'f', 0, 64),
		strconv.FormatFloat(mat.Sum(ecc)/float64(vertices), 'f', 3, 64),
		fmt.Sprintf("%d / %d = %.2f", p, l, float64(p)/float64(l)),
		spect.String(),
		strconv.Itoa(spect.Components()),
	}
}

// main takes a length dimension for generating matrices on which to perform calculations and an optional file name.
// The generated matrices and the results of any calculations are output to a csv file with one matrix per row.
func main() {
	logger := log.New(os.Stdout, "Research: ", log.LstdFlags)
	length, err := getArgLength()
	logErr(logger, err)

	filename, err := getArgFileName()
	logErr(logger, err)

	f, err := os.Create(filename)
	logErr(logger, err)
	defer f.Close()

	w := csv.NewWriter(f)

	err = initCSV(w)
	logErr(logger, err)

	g := matgen.NewSymGen(length)

	for i := 0; i < g.Len(); i++ {
		err = w.Write(adjCSVRecord(adjmat.NewSymAdj(g.Generate())))
		logErr(logger, err)
	}

	err = closeWriter(w)
	logErr(logger, err)
}
